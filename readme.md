# Play Creeper World in real-world terrain!
If this project works, it will let you play creeper world missions on maps derived from real-world data (from Google Maps). The project is not yet complete. USE AT YOUR OWN RISK!

# Status
Very much in progress. Still to do:

- Make javascript library / external files
- Wait for CW3 to come out and make maps in that format
- Performance - can I do faster than 30sec to view data?
- Download imagery too?

# Pretty Pictures
Here's what the flow looks like.

First, select an interesting region in google maps. ![map](https://bitbucket.org/aguakandra/elevation-from-google-maps/raw/default/image/map.png "This one is a volcano somewhere")

Next, grab the elevation data. ![elevations](https://bitbucket.org/aguakandra/elevation-from-google-maps/raw/default/image/elevations-bw.png "OMG it works!")

If black and white is too boring, go with a custom color mix. ![map](https://bitbucket.org/aguakandra/elevation-from-google-maps/raw/default/image/elevations.png "Something like aqua,black,red,white")

Then quantize to 10 levels. ![quantized](https://bitbucket.org/aguakandra/elevation-from-google-maps/raw/default/image/levels.png "Just like a map!")
