// understand the quantization process better with graphics
// depends on HighCharts

// TODO
// just plot breakpoints, maybe with step line plot
// plot column chart of relative amounts
// maybe add live multi slider

function QuantDisplay(chartNode, data) {
  console.time('construct QuantDisplay');
  this.chartNode = chartNode;
  this.data = data;
  this.setQuantColors = function(quantizer, colors) {
    this.quantizer = quantizer;
    this.quant_colors = colors;
  };
  this.quantizer = null;
  this.quant_colors = null;

  var stdcolors = Highcharts.getOptions().colors;
  this.chart = new Highcharts.Chart({
    chart: {
      type: 'area',
      animation:false,
      renderTo: this.chartNode
    },
    title: {text: 'Elevation Distribution'},
    yAxis: [{
      title: {text: 'Rank (% of total or % of max)'},
      min:-102,
      max:102,
      startOnTick:false,
      endOnTick:false,
      minorTickInterval:'auto',
      labels: {
        formatter: function() {
          return Math.abs(this.value);
        }
      }
    }],
    xAxis:[{
      title: {text: 'Elevation Value'},
      id: 'mainaxis'
    }],
    tooltip:{
      crosshairs:[true,false],
      formatter: function() {
        var s = this.x.toFixed(1) + '<br/>';
        var levelp;
        this.points.forEach(function(p) {
          if(p.series.name.charAt(0) === "Q") {
            levelp = p;
          } else {
            s += '<span style="color:'+p.series.color+'">'+p.series.name+'</span>: <b>'+p.point.y.toFixed(1)+'%</b><br/>';
          }
        });
        if (typeof levelp !== 'undefined') {
          var levelindex = levelp.series.name.substr(1);
          // ensure that all points in the series show the same y-value, event the two hacky 0-valued ones.
          var levelpct = Math.abs(levelp.series.dataMin);
          s += 'Quantized Level '+levelindex+': <b>'+levelpct.toFixed(1)+'%</b>';
          
        }
        return s;
      },
      shared:true
    },
    plotOptions: {
      line : {
        marker:{enabled:false},
        step:'center'
      },
      area : {
        marker:{enabled:false},
        step:'center'
      },
      series : {
        animation: false
      }
    },
    series: [{
      id:'cdf',
      name:'Original Percentile',
      type:'line',
      index:1,// top
      legendIndex:1,
      color:stdcolors[3]
    },{
      id:'pdf',
      name:'Original Density',
      index:0,
      legendIndex:0,
      color:stdcolors[0]
    }]
  });
  var numerically = function(a,b) {
    return a-b;
  }
  var custom_series_ids = [];
  this.showDataPlots = function() {
    console.time('quant data plot');
    var flat_data = this.data.toArray().sort(numerically);
    var N = flat_data.length;

    var xvalues = flat_data;
    var cum_percent = _.range(flat_data.length).map(function(i) {return i / flat_data.length * 100});

    // determine optimal number of bins to group for the histogram by a magic formula
    var bins = 40;
    if (bins > N) bins = N;
    if (bins < Math.sqrt(N)) bins = Math.floor(Math.sqrt(N));

    var histdata = this.data.quantizeLinear(bins).toArray().sort(numerically);
    var histcounts = histdata.map(look_up_count(make_count(histdata)));
    var histdensity = normalize_to(histcounts, 100);

    this.chart.get('cdf').setData(_.zip(xvalues,cum_percent), false);
    this.chart.get('pdf').setData(_.zip(xvalues,histdensity), false);

    this.chart.redraw();
    console.timeEnd('quant data plot');
  }
  this.showQuantization = function() {
    if (!this.quantizer) return;
    console.time('quant plot');
    var Nb = this.quantizer.breakpoints.length;
    var flat_data = this.data.toArray().sort(numerically);
    var N = flat_data.length;

    var xvalues = flat_data;

    var qdata = this.data.quantizeWith(this.quantizer).toArray().sort(numerically);
    var qbincounts = make_count(qdata);
    var qcounts = qdata.map(look_up_count(qbincounts));
    var qdensity = normalize_to(qcounts, 100);
    qdensity = qdensity.map(negate);

    // remove old series
    while(custom_series_ids.length) this.chart.get(custom_series_ids.pop()).remove(false);

    var qxd = _.zip(xvalues,qdensity);

    // add new series, one per quantized level
    for(var i = 0, indexA=0, indexB=0; i < Nb+1; i++, indexA = indexB) {
      var level_val = qdata[indexA];
      indexB = indexA + qbincounts[level_val];
      // console.log("level " + (i+1) + " (" + level_val + ") goes from " + indexA + " to " + indexB );
      var id = _.unique('qseries');
      var d = [[xvalues[indexA],0]].concat(qxd.slice(indexA, indexB)).concat([[xvalues[indexB-1],0]]);
      this.chart.addSeries({
        name : "Q"+(i+1),
        data : d,
        legendIndex : 2+i,
        id: id,
        color: this.quant_colors[i],
        lineColor: 'black',
        marker:{
          states:{hover:{enabled:false}}
        }
      },
      false);
      custom_series_ids.push(id);
    }
    this.chart.redraw();
    console.timeEnd('quant plot');
  }
  console.timeEnd('construct QuantDisplay');
}
function look_up_count(bin_counts) {
  return _.partial(_.result,bin_counts);
}
function make_count(data) {
  return _.countBy(data, _.identity);
}
function normalize_to (data, maxvalue) {
  maxvalue = maxvalue || 1;
  var scale = maxvalue / _.max(data);
  return data.map(function(x) {
    return x*scale
  });
}
function negate(x) {
  return -x;
}
