Outline



this all revolves around the data

data is a 2d array of values, probably square but not always
values are numbers
- type raw is floats of any size
- type quantized has integer types, with bounded range. Usually 1-10 inclusive.

Performance: worry about that later. i'm not doing anything fancy.


so maybe ElevationData = {
	set : function(r,c, val) {this[r][c] = val}
	get : function(r,c) {return this[r][c]} // might be undef
	min : function() {...}
	max : function() {...}
	each : function(callback) {callback(val)}
	values : function() {
		// return array of different values if quantized
		// return undef otherwise
	}
}

and elsewhere
ElevationData.prototype.quantizeLinear = function(# of values) {
	// don't modify this, return new ElevationData object
}


Ok, I also need to plot the data
This involves drawing to a canvas
Maybe also saving to image or cw3 file

But primarily render to canvas.
When to render?
- on demand
- automatically
   need listeners, notification, all that crap

Shoot for just on demand
So DataRenderer = {
	DataRenderer(canvas node) {...}
	data : null
	other properties, like scaling (resize to 8x vs fit to current)
	render : function(){draw stuff}

}


Map Fetcher thingy - encapsulate the map manipulation,
sample elevation with callbacks
fetchElevation = function(bounds, updateCallback, doneCallback, errorCallback)
look at http://api.jquery.com/jQuery.Deferred/
return the promise
do elev fetch, then more or resolve or reject.
notify after each step

user of fetcher can bind notify to redraw map
unknown values should be undefined
- and plotter should handle that


NEXT UP
add slider thingy, see if I can get live update to the image working, then (semi)live update to the chart
check if it works for undefined
add quantize by ... buttons (linear, equalize, autoGroup)

THEN
Map Fetcher, timing, etc.

THEN
map control modules, feedback, etc.

THEN
diagonal renderer
