// Adjust quantization levels

function QuantController(node,data,quantizer,colors) {
  this.$node=$(node);
  this.$node.empty();
  this.$node.append('<h2>Quantization Control</h2>');
  this.$slider=$('<div>').appendTo(this.$node);
  this.$node.append('<h3>Auto-assign Level Cutoffs:</h3>');
  this.$node.append('<button id="btn-linear">Linear Spacing</button>');
  this.$node.append('<button id="btn-equalize">Equalize Distribution</button>');
  this.$node.append('<button id="btn-cluster">Cluster</button>');
  $('button', this.$node).button();
  this.data = data;
  this.quantizer = quantizer;
  this.colors = colors;

  var callbacks = [];
  this.add_callback = function(callback, max_interval) {
    if (max_interval) callbacks.push(_.debounce(callback, max_interval));
    else              callbacks.push(callback);
  };
  this.notify = function() {
    callbacks.forEach(function(x){x.call()});
  }

  var that=this;
  var lo = data.min(), hi = data.max();
  var update = function (event, ui) {
    var which = $(ui.handle).index();
    var ok = checkSepFor(ui.values, which, 1);
    if (ok) {
      // $('#values').html(ui.values.toString());
      // console.log(ui.values);
      makeStops(that.$slider, toPercent(lo,hi,ui.values), that.colors);
      _.defer(that.notify);
    }
    return ok;
  };
  // init slider
  this.$slider.slider({
    min: lo,
    step: 1,
    max: hi,
    values: quantizer.breakpoints,
    slide: update,
    change: update
  });
  // done
  makeStops(this.$slider, toPercent(lo,hi,quantizer.breakpoints), colors);

  var that = this;
  this.calcLinear = function() {
    that.$slider.slider('values',GetLinearScale(that.data.min(), that.data.max(), that.quantizer.breakpoints.length+1));
  };
  this.calcHistEqualize = function() {
    that.$slider.slider('values',GetEqualizedScale(that.data.toArray(), that.quantizer.breakpoints.length+1));
  }
  $('#btn-linear').click(this.calcLinear);
  $('#btn-equalize').click(this.calcHistEqualize);
  $('#btn-cluster').click(this.calcCluster);
}

function checkSepFor(array, checkIndex, minSep) {
    if (checkIndex > 0 && array[checkIndex] - array[checkIndex - 1] < minSep) return false;
    if (checkIndex < array.length - 1 && array[checkIndex + 1] - array[checkIndex] < minSep) return false;
    return true;
}
function toPercent(lo,hi,inBetween) {
  return inBetween.map(function(x) {
    return (x-lo)/(hi-lo)*100;
  });
}
function makeStops($slider, stops, colors) {
    if (stops.length != colors.length-1) throw new Error();
    var prefixes = ['-webkit-', '-moz-', '-ms-', '-o-'];
    var gradstr = 'linear-gradient(left';
    for (var i = 0; i < colors.length; i++) {
        var stopL = (i == 0) ? 0 : stops[i-1];
        var stopR = (i == colors.length-1) ? 100 : stops[i];
        gradstr += ", " + colors[i] + " " + stopL + "%";
        gradstr += ", " + colors[i] + " " + stopR + "%";
    }
    gradstr += ')';
    // apply with browser-specific prefixes
    for (var i = 0; i < prefixes.length; i++) {
        $slider.css('background-image', prefixes[i]+gradstr);
    }
}