// Quantization stuff
// depends on _


// Quantizers have a list of break points.
// for N different values, these are the N-1 intermediates
// below the first is 0, above (>) the i'th is i.
// the breakpoints are not always evenly spaces, but are increasing.
function Quantizer (breakpoints) {
  this.breakpoints = breakpoints || [0];
  var that = this;
  this.quantize = function(x) {
    return _.sortedIndex(that.breakpoints, x);
  };
}

// make am evenly-distributed scale
function GetLinearScale (lo, hi, N) {
  var points = [], incr = (hi-lo)/N;
  for (var i = 1; i <= N-1; i++) 
    points.push(lo + incr * i);
  return points;
}

// set scale to (hopefully) equalize the histogram - equal numbers in each band.
function GetEqualizedScale(data, N) {
	var sorted = data.sort(function(a,b){return a-b});
	var lookups = GetLinearScale(0,data.length-1,N);
	return lookups.map(function(x){
		return sorted[Math.floor(x)]
	});
}