// Depends on:
// Rainbow
// jQuery

function DataRenderer(canvasNode, data) {
  var defaultColors = ['black', 'white'], colors = defaultColors;

  this.canvasNode = canvasNode;
  this.data = data;
  this.square = true; // draw cells as squares or stretch to fit
  this.quantizer = null;
  Object.defineProperty(this, "colors", {
    get: function() {return colors;},
    set: function(c) {
      try {
        new Rainbow().setSpectrumByArray(c);
      } catch(e) {
        throw new TypeError('invalid colors '+c);
      }
      colors = c;
    },
    configurable: false,
    enumerable: true
  });

  this.resizeToFit = function(scale) {
    scale = scale || 8; // default 8 pixels per square
    // TODO resize canvas
  };

  // return N different colors
  this.GetQuantColorRange = function() {
    if (this.quantizer === null) return this.colors;
    var N = this.quantizer.breakpoints.length+1;
    colormap = new Rainbow();
    colormap.setSpectrumByArray(this.colors);
    colormap.setNumberRange(0,N-1);
    return _.range(0,N).map(function(i){return "#" + colormap.colorAt(i);});
  }

  // draw the data to the canvas node
  // determines scale to fit.
  this.render = function() {
    console.time('render');
    var R = this.data.rows, C = this.data.cols;
    var W = $(canvasNode).width(), H = $(canvasNode).height();
    var ww = Math.floor(W/C), hh = Math.floor(H/R);
    if (this.square) {
      ww = hh = Math.min(ww, hh);
    }
    var ctx = canvasNode.getContext("2d");
    var lo,hi;
    if (this.quantizer !== null) {
      lo = 0; hi = this.quantizer.breakpoints.length;
    } else {
      lo = this.data.min(), hi = this.data.max();
    }

    ctx.clearRect(0,0,W,H);
    colormap = new Rainbow();
    colormap.setSpectrumByArray(this.colors);
    colormap.setNumberRange(lo, hi);
    console.log("drawing with values in range " + lo + " to " + hi + " and colors " + this.colors);

    var mapfn = this.quantizer === null ? _.identity : this.quantizer.quantize;
    this.data.forEach(function(val,r,c) {
      val = mapfn(val);
      ctx.fillStyle = '#' + colormap.colorAt(val);
      var xx = c*ww, yy = r*hh;
      ctx.fillRect(xx, yy, ww, hh);
    }, this);
    console.timeEnd('render');
  }
}
