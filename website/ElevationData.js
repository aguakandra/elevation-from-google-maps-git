function ElevationData(rows, cols) {
  var data, _rows, _cols;

  // Set, Get, and sizes
  this.set = function(r,c,val) {
    if (r<0||r>=this.rows||c<0||c>=this.cols)
      throw new Error("index "+r+","+c+" out of bounds for "+this);
    data[r][c] = val;
    return this;
  };
  this.get = function(r,c) {
    if (r<0||r>=this.rows||c<0||c>=this.cols)
      throw new Error("index "+r+","+c+" out of bounds for "+this);
    return data[r][c];
  };

  Object.defineProperty(this, "rows", {
    get: function() {return _rows;},
    configurable: false,
    enumerable: true
  });
  Object.defineProperty(this, "cols", {
    get: function() {return _rows;},
    configurable: false,
    enumerable: true
  });

  // (re)set the data to a rectangular array of rows x cols, each value being val.
  // defaults are 1x1 filled with zero
  this.init = function(rows, cols, val) {
    rows = Math.max(rows || 1, 1); // default 1 minimum 1
    cols = Math.max(cols || 1, 1); // default 1 minimum 1
    val = typeof(val) === "undefined" ? 0 : val; // default 0, can be null


    // fill in empty rows
    data = [];
    while(data.push([]) < rows);
    // fill in values
    var r,c;
    for (r = 0; r < rows; r++) {
      for (c = 0; c < cols; c++) {
        data[r][c] = val;
      }
    }
    _rows = rows;
    _cols = cols;
    return this;
  };

  // minimum value
  this.min = function() {
    var r,c;
    var m=data[0][0];
    for (r = 0; r < _rows; r++) {
      m = Math.min(m, Math.min.apply(null, data[r]));
    }
    return m;
  };
  // maximum value
  this.max = function() {
    var r,c;
    var M=data[0][0];
    for (r = 0; r < _rows; r++) {
      M = Math.max(M, Math.max.apply(null, data[r]));
    }
    return M;
  };

  // iterator stuff
  // Do callback.call(scope, item, row, column, ElevationData object)
  this.forEach = function(callback, scope) {
    for (r = 0; r < _rows; r++) {
      for (c = 0; c < _cols; c++) {
        callback.call(scope, data[r][c], r, c, this);
      }
    }
  };

  // toArray and fromArray
  this.toArray = function() {
    var array = [];
    this.forEach(function(x){array.push(x);});
    return array;
  };
  this.fromArray = function(array, rows, cols) {
    if (array.length === 0) throw new Error("input is empty");
    if (array.length !== rows * cols) throw new Error("rows and cols don't match up");

    // fill in empty rows
    data = [];
    while(data.push([]) < rows);
    // fill in values
    var r,c,i=0;
    for (r = 0; r < rows; r++) {
      for (c = 0; c < cols; c++) {
        data[r][c] = array[i++];
      }
    }
    _rows = rows;
    _cols = cols;
    return this;
  };

  this.toString = function() {
    return "[ElevationData of " + _rows + " rows x " + _cols + " cols]";
  };


  this.init(rows, cols);
}

ElevationData.prototype.quantizeWith = function(quantizer) {
  quantizer = quantizer || function(x) {return x};

  var qdata = new ElevationData(this.rows, this.cols);
  this.forEach(function(x,r,c){
    qdata.set(r,c, quantizer.quantize(x));
  })
  return qdata;
};

ElevationData.prototype.quantizeLinear = function(N) {
  return this.quantizeWith(new Quantizer(
    GetLinearScale(this.min(), this.max(), N)
  ));
}
